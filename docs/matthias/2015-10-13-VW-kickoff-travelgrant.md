---
layout: post
title:  "Application VW kickoff: Cells in suspended animation - when life takes a break"
categories: [lab-book]
tags: [grants]
---

## Abstract

Cells can enter into a state of suspended animation, or dormancy, when faced with unfavorable conditions. In this state, cells can neither be categorized as alive, because classical definitions of life like metabolism, growth and reproduction do not apply anymore, nor as dead, because they can continue to grow when conditions improve. How cells enter into and recover from this state is still poorly understood. Here, we study suspended animation in different eukaryotic organisms and show that when we induce this state through energy depletion, many cytoplasmic proteins - among them key metabolic enzymes -  form macromolecular assemblies and become deactivated. This goes along with a transition of the cytoplasm from a liquid to a solid-like state, changes in the global mechanical properties of the cell and a loss of cellular water. This transition to a solid-like state is triggered by a marked decrease in cytosolic pH which, as we could show, is also essential for the survival of dormancy. Our findings have broad implications for understanding alternative physiological conditions, such as dormancy and quiescence and create a new view of the cytoplasm as an adaptable fluid that can reversibly transition into a protective solid-like state.


## Description of research focus and value for symposium

Our research focuses on the fascinating idea that a global reorganization of living matter is required for the transition between an "active" and a "suspended animation" state of life. We hypothesize that, although the triggers that send cells into this state are very diverse, the final state could have some unifying characteristics like an assembled and therefore protected proteome, low water content and pH as well as solid-like mechanical properties.

Our research fits very well to the overall scope of the "Life?" funding initiative because we explore some very basic biological principles that lead us into areas where the question "What is life?" becomes very difficult to answer. Cells in suspended animation can, in extreme cases, not be described by the conventional definitions of life like metabolism, growth, replication and proliferation. Nonetheless, they are not dead either as they can continue to grow and divide when conditions improve. Our research provides molecular explanations for the existence of this state and thereby contributes to a more accurate description of life. We think that it therefore is of great value for the symposium in Hanover.
