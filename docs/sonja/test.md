# Building a 3D PDMS device

![Lausanne](../img/Lausanne.jpg)

This week I was lucky enough to visit the [Amstad lab](http://smal.epfl.ch/home) in beautiful Lausanne (see image above) to learn how to build a PDMS device for production of single and double emulsions. This post was first intended to document the visit for myself only (I'm running a Jekyll-based lab-notebook locally) but I thought it might be helpful to others as well, so here it is.

<!---
I first wrote this post to document everything for myself (I'm running a Jekyll-based lab-notebook locally) but then I decided to generalize it and put it online to share my experience. Enjoy!

*Ask Esther for permission to post this*
-->

## Chip and photomask design

The first step in the process is the software-based design of the device. People are usually using professional software like [AutoCAD](http://www.autodesk.de/products/autocad/free-trial), [Sketchup](http://www.sketchup.com/) etc. for this task. I was not able to learn much about this part because we started with a finished, printed photomask. So if you're particularly interested in photomask design this post is not for you.


## Printing the photomask

The [photomask](https://en.wikipedia.org/wiki/Photomask) can be printed either as a chrome mask or as a foil mask. High resolution printing is essential for the quality of the device. Printing is therefore usually carried out by specialized companies like???. Give some examples plus links...


## Master production

A so called master or mold is produced in a clean room environment from a silicon wafer which is used as carrier (or substrate). A thin layer of photoresist - in our case [SU-8](https://en.wikipedia.org/wiki/SU-8_photoresist), a negative photoresist -  is spin coated onto the wafer. The spinning speed and timing, as well as the viscosity of the photoresist (there is different types of SU-8 for example) determine the thickness of the photoresist layer.

Because we are producing a 3 dimensional device with 2 different channel heights, spin-coating, baking, and UV-exposure have to be repeated twice as described below.

In the clean room:

- Clean the silicon wafer with oxygen plasma for 7 minutes
- Clean the photomask as well as a glass plate (which will later be used to fix the mask to the wafer) with isopropanol and dry both with a nitrogen gun
- Apply SU-8 (3050) to the clean wafer, avoid air bubbles and run spinning protocol. I'm not documenting the spinning settings here because there probably are different centrifuges in Dresden anyway. The goal is a layer of photoresist that is 60 μm thick!
- Soft bake the photoresist for 25 min at 90˚C
- Let it cool down on a RT surface for a couple of minutes
- Firmly attach the photomask to the wafer using sticky tape and the glass plate. In the right orientation, looking from the top, the text on the mask must be readable and the glass then has to go on top of it. Make sure the tape is outside the chip area.
- Apply UV light. Salvatore should know about intensity and time i.e. the suitable UV dose...
- After UV exposure the photoresist is hard baked, in our case 1 min at 65˚C followed by 5 min at 90˚c.
- Now it's time to apply the second layer of photoresist, again by spin-coating. Another type of SU-8 - SU-8 3025 - is used this time and the spinning protocol is adjusted to generate a 40 μm thick layer
- Repeat soft bake, UV exposure and hard bake and let the wafer cool to room temperature afterwards
- Finally the wafer is developed by putting it into ???. This chemical dissolves all photoresist that has not been cross-linked by UV light. The remaining photoresist (different height for different channels because of the 2 step procedure!) later serves as a positive for production of the PDMS device.

The last working step in the cleanroom is the surface treatment of the master with TMCS (stands for ???) which helps to prevent the PDMS from sticking to the mold later:

- Under a fume hood place the silicon/SU-8 mold into a special TMCS desiccator
- Place a few drops of TMCS into a tiny beaker inside the TMCS desiccator (use single pipettes for that purpose)
- Close the desiccator and place it under vacuum dor 35 min. This causes the TMCS to evaporate and to form a passivation layer on the mold surface.


## Mixing, pouring and degassing the PDMS

Back in the normal lab, the PDMS has to be mixed from base PDMS and a catalyst for polymerization in a 10:1 ratio. This is done like follows:

- Put an empty and clean single use plastic cup on the precision scale
- Tare the scale so it displays 0
- Add the base PDMS (max 45 g) and write down the value
- Tare the scale so it displays 0
- Using a pipette, add the catalyst (max 4.5 g) to reach the ratio value
- Mix very well, then pour into the mold. About 10 grams should be enough to cover one mold, so adjust PDMS amount according to how many molds should be filled...
- Degas by putting everything into a vacuum chamber/desiccator for 20 minutes.
- Let the degassed PDMS harden over night at 70˚C.


## Punching holes for inlets and outlets

The device we produced during this week has the following layout (taken from [Arriaga et al.](https://www.researchgate.net/profile/Esther_Amstad)):

![Layout of the PDMS device for production of double emulsions](../img/chip_layout.png "Layout of the PDMS device for production of double emulsions")
*Figure 1: Layout of the PDMS device for production of double emulsions.*

In Figure 1 the upmost panel shows an outer, middle and inner inlet (indicated by downward arrows) as well as an outlet (indicated by an upward arrow). In all of these position a hole has to be punched into the PDMS prior to bonding. This hole will later be used to connect the tubing coming from the syringe pumps (for the inlets) and the tubing to collect the double emulsion (from the outlet). Because we are producing a 3 dimensional device we have 2 pieces of PDMS which will be bonded together (see below) to form the complete device. Holes, of course, only have to be punched into one side of the device. In figure 2 A you can see the PDMS that has just been cut out and peeled of the master. The right side ??? is the top and the left side is the bottom ??? of the device. Figure 2 B showes the device after the holes have been punched.

Biopsy needles [like this](https://www.tedpella.com/histo_html/miltex-plunger-punch.htm) can be used to punch the holes.

## Bonding

In this step the PDMS has to be bonded either to a glass surface (coverslip or similar) in the case of a 2-dimensional device or two pieces of PDMS have to be bonded together in the case of a 3-dimensional device. Here we cover the case of a 3-dimensional device.

The surface of the PDMS has to be cleaned to make sure no grease, dust etc. will affect the bonding process later. To achieve this the surface is rinsed with pure isopropanol, dried with the nitrogen gun and subsequently covered with sticky tape (Tesa film). It can be stored like that if needed. Subsequently the sticky tape is removed and the PDMS is cut into the 2 halves which will form the device when bonded together.

One way to achieve bonding is by treating the PDMS surface with oxygen plasma. Plasma oxidization leads to the formation of silanol groups on the PDMS surface. When 2 such surfaces are brought into close contact covalent –O-Si-O- bonds will form and connect the two surfaces (see this [book chapter](https://gmwgroup.harvard.edu/pubs/pdf/1073.pdf) for a very good description).

The surfaces have to be brought in contact immediately after plasma treatment for the bonding to work. In the case described here, the channels of the microfluidic device have to be aligned properly. To achieve this a small water drop is put on one of the PDMS surfaces before bringing the surfaces together. The resulting thin water film between the surfaces allows to slide the PDMS pieces with respect to each other for proper alignment of the channels. Alignment is done under a binocular. Tip: if the water doesn't wet the plasma treated PDMS surface easily the plasma treatment failed and has to be repeated or the setting for the plasma treatment have to be optimized.

Put the aligned PDMS device into the 70˚C incubator over night for bonding. Don't touch, shake etc. the device during that to avoid disturbing the alignment.

## Functionalizing the different parts of the device

This step is absolutely crucial to get a properly working device. The different parts of the device have to be rendered either hydrophobic (here also fluorophillic) or hydrophillic so that either the oil phase or the water phase can wet the channel walls. Figure 1 is very helpful to understand the following step by step description:

- Flush the complete device with 1 M NaOH using a 5 (10) ml syringe equipped with these [needles](http://www.vitaneedle.com/disposable-needles-syringes.htm) connected to the inlet of the outer channel
- Wait for 5 minutes
- Wash out the NaOH with H2O using a similar pipette + tip
- Dry the device with a nitrogen gun
- Flush the outer channel with PDADMAC (see the chemica list for full name) - a cationic polyelectrolite to render the outer channel walls as well and the blue part of the chip (see Figure 1) hydrophyllic.
- Incubate for 30 min
- Wash device with water and dry with nitrogen gun
- No comes the hardest part: Flush the middle channel with a solution made from 1 ml HFE + 20 μl fluorinated trichloro-silane. Do this very carefully so that the liquid only reaches the end of the tip (green region in Figure one) and does not touch the downstream channel walls (blue area).
- Incubate for 20 min
- Wash middle channel with HFE and dry with nitrogen gun

The chip is now ready to be operated

## Operating the device

To observe the device during operation it is put onto a big coverslip and put onto a microscope stage. A 10x lens is suitable to observe the tip.

On the stage connect the tubing coming from 3 different syringe pumps (outer, middle, inner) with the device. For this pull the metal part from one of these needles:

![Needles](../img/Needles.jpg)

<!---
<p align="center">
<img src="/images/Needles.jpg" alt="Needles" style="width: 300px;"/>
</p>
-->

connect it to the tubing and stick it into the corresponding inlet on the PDMS device. To quickly get to a stable operating mode, make sure there are no air bubbles in the tubing i.e. the liquid has reached the end of the metal tubing before you connect it.

Finally connect a tube without metal tip to the outlet and collect the double emulsion in a glass vial or Eppendorf tube.

Flow rates are very important to get to a stable operation mode. A the following values are a good starting point:

Start by switching on the outer and middle phase only with the following flowrates:

- outer: 4000 μl/h
- middle: 400 μl/h

Once a stable single emulsion mode is achieved, switch on the inner phase

- inner: 1400 μl/h

Let the flows equilibrate for several minutes and test if a stable double emulsion mode can be achieved. Play around with the flow parameters when necessary. If that doesn't help the surface treatment most likely failed and you have to switch to another device.

## Chemical and hardware list

These things have to be ordered in Dresden.

### Chemicals:

- HFE-7500 (3M NOVEC Engineered Fluid) - order 1 liter from ???. Look for good source.
- Surfactant Krytox-PEG-Krytox. I already got it from Gianluca but check for commercial supplier anyway
- poly(diallyldimethylammonium chloride) (400–500 kDa) - order 1 l of 20 % solution from Sigma:

![PDADMAC](../img/PDADMAC.png)

- Trichloro(1H, 1H, 2H, 2H-perfluoro-octyl)silane - order 10 g from Sigma:

![trichloro-silane](../img/trichloro-silane.png)


### Hardware:

- Tubing:

![tubing](../img/tubing.png)

- [Needles](http://www.vitaneedle.com/disposable-needles-syringes.html)
- [Biopsy punch](https://www.tedpella.com/histo_html/miltex-plunger-punch.htm)


# Resources and references

- See this very helpful [description](https://cmi.epfl.ch/packaging/PDMS.html) on the EPFL CMI website
- This [book chapter](https://gmwgroup.harvard.edu/pubs/pdf/1073.pdf) is an excelent resource to learn about microfluidic PDMS devices
- This [paper](https://www.researchgate.net/profile/Esther_Amstad) is about exactly the device I was working on.
